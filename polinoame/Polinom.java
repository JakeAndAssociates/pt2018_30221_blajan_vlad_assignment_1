	package polinoame;

import java.util.*;
import java.util.Map.Entry;

public class Polinom {

	public Map<Integer, Monom> polinom = new HashMap<Integer, Monom>();
	
	
	public Polinom()
	{
		
	}
	
	public Polinom(String str)
	{
		parsePolinom(str);
	}
	
	public void parsePolinom(String str)
	{
		boolean minusFlag = false;
		boolean coefFlag = false;
		
		double coef = 1;
		
		polinom.clear();
		
		for(String val : str.split("x\\^|\\ | ")) 
		{
			if(val.equals("-"))
			{
				minusFlag = true;
				continue;
			}
			
			if(val.equals("+"))
				continue;
			
			if(coefFlag == false)
			{
				coef = Double.parseDouble(val);
				if(minusFlag)
				{
					coef= -coef;
					minusFlag = false;
				}
				coefFlag = true;
			}
			else
			{
				if(coef != 0.0)
					polinom.put(Integer.parseInt(val), new Monom(Integer.parseInt(val),coef));
				coefFlag = false;
			}
		}	
	}
	
	public String toString()
	{
		String rez = new String("");
		
		List<Integer> keys = new ArrayList<Integer>(polinom.keySet());
		Collections.sort(keys);
		Collections.reverse(keys);
		
		for(int itter : keys)
		{
			if(polinom.get(itter).getCoef() == 0)
				continue;
			if(polinom.get(itter).getCoef() > 0)
			{
				rez += "+";
				System.out.print("+");
			}
			System.out.print(polinom.get(itter).getCoef() + "x^" + itter);
			rez += polinom.get(itter).getCoef() + "x^" + itter;
			
		}
		
		if(rez.equals(""))
			rez += "0";
		
		System.out.println();
		return (rez);
	}
	
	public Polinom add(Polinom other)
	{
		Polinom rez = new Polinom();
		
		Iterator<Entry<Integer, Monom>> it = this.polinom.entrySet().iterator();
		
		while(it.hasNext())
		{
			Map.Entry<Integer, Monom> pair = (Map.Entry<Integer, Monom>)it.next();
			if(other.polinom.containsKey(pair.getKey()))
			{
				rez.polinom.put(pair.getKey(), new Monom(pair.getKey(), pair.getValue().getCoef() + other.polinom.get(pair.getKey()).getCoef()));
			}
			else
			{
				rez.polinom.put(pair.getKey(), pair.getValue());
			}
		}
		
		
		it = other.polinom.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry<Integer, Monom> pair = (Map.Entry<Integer, Monom>)it.next();
			if(!rez.polinom.containsKey(pair.getKey()))
			{
				rez.polinom.put(pair.getKey(), pair.getValue());
			}
		}
		return rez;
	}
	
	public Polinom dif(Polinom other)
	{
		Polinom rez = new Polinom();
		
		Iterator<Entry<Integer, Monom>> it = this.polinom.entrySet().iterator();
		
		while(it.hasNext())
		{
			Map.Entry<Integer, Monom> pair = (Map.Entry<Integer, Monom>)it.next();
			if(other.polinom.containsKey(pair.getKey()))
			{
				rez.polinom.put(pair.getKey(), new Monom(pair.getKey(), pair.getValue().getCoef() - other.polinom.get(pair.getKey()).getCoef()));
			}
			else
			{
				rez.polinom.put(pair.getKey(), pair.getValue());
			}
		}
		
		
		it = other.polinom.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry<Integer, Monom> pair = (Map.Entry<Integer, Monom>)it.next();
			if(!rez.polinom.containsKey(pair.getKey()))
			{
				rez.polinom.put(pair.getKey(), new Monom(pair.getKey(), -pair.getValue().getCoef()));
			}
		}
		return rez;
	}
	
	public Polinom mul(Polinom other)
	{
		Polinom rez = new Polinom("0x^0");
		
		if(polinom.isEmpty() || other.polinom.isEmpty())
			return rez;
		
		Iterator<Entry<Integer, Monom>> firstIt = this.polinom.entrySet().iterator();
		
		while(firstIt.hasNext())
		{
			Map.Entry<Integer, Monom> firstPair = (Map.Entry<Integer, Monom>)firstIt.next();
			Iterator<Entry<Integer, Monom>> secondIt = other.polinom.entrySet().iterator();
			
			while(secondIt.hasNext())
			{
				Map.Entry<Integer, Monom> secondPair = (Map.Entry<Integer, Monom>)secondIt.next();
				if(rez.polinom.containsKey(firstPair.getKey() + secondPair.getKey()))
					rez.polinom.replace(firstPair.getKey() + secondPair.getKey(), new Monom(firstPair.getKey() + secondPair.getKey(), rez.polinom.get(firstPair.getKey() + secondPair.getKey()).getCoef() + firstPair.getValue().getCoef() * secondPair.getValue().getCoef()));
				else
					rez.polinom.put(firstPair.getKey() + secondPair.getKey(), new Monom(firstPair.getKey() + secondPair.getKey(), firstPair.getValue().getCoef() * secondPair.getValue().getCoef()));
			}
		}
		return rez;
	}
	
	public int maxGrad()
	{
		List<Integer> keys = new ArrayList<Integer>(polinom.keySet());
		Collections.sort(keys);
		Collections.reverse(keys);
		
		return keys.get(0);
	}
	
	private Polinom divide(Polinom impartitor, Polinom deimpartit)
	{
		Polinom rez = new Polinom("0x^0");
		
		while(deimpartit.maxGrad() >= impartitor.maxGrad())
		{	
			double coef = deimpartit.polinom.get(deimpartit.maxGrad()).getCoef() / impartitor.polinom.get(impartitor.maxGrad()).getCoef();
			int grad = deimpartit.maxGrad() - impartitor.maxGrad();
			
			rez.polinom.put(grad, new Monom(grad, coef));
			
			Polinom auxImp = new Polinom();
			Polinom inmultire = new Polinom();
			
			auxImp.polinom.putAll(impartitor.polinom);
			inmultire.polinom.put(grad, new Monom(grad, coef));
			
			auxImp = auxImp.mul(inmultire);
			deimpartit = deimpartit.dif(auxImp);
		
			Iterator<Entry<Integer, Monom>> firstIt = deimpartit.polinom.entrySet().iterator();
		
			while(firstIt.hasNext())
			{
				Map.Entry<Integer, Monom> pair = (Map.Entry<Integer, Monom>)firstIt.next();
			
				if(pair.getValue().getCoef() == 0.0)
				{
					deimpartit.polinom.remove(pair.getKey());
					firstIt = deimpartit.polinom.entrySet().iterator();
				}
			}
		
			firstIt = deimpartit.polinom.entrySet().iterator();
		
			if(!firstIt.hasNext())
				return (rez);

		}
		return(rez);
	}
	
	public Polinom div(Polinom other)
	{
		Polinom rez = new Polinom("0x^0");
	
		if(polinom.isEmpty() || other.polinom.isEmpty())
			return rez;
			
		int deimpartit = this.maxGrad();
		int impartitor = other.maxGrad();
		
		if(deimpartit < impartitor)
			return rez;
		
		Polinom impartitorPol = new Polinom();
		Polinom deimpartitPol = new Polinom();
		
		impartitorPol.polinom.putAll(other.polinom);
		deimpartitPol.polinom.putAll(this.polinom);
		
		rez =  this.divide(impartitorPol, deimpartitPol);
		
		return rez;
	}
	
	public Polinom deriv()
	{
		Polinom rez = new Polinom();

		Iterator<Entry<Integer, Monom>> it = this.polinom.entrySet().iterator();
		
		while(it.hasNext())
		{
			double coef;
			int grad;
			Map.Entry<Integer, Monom> pair = (Map.Entry<Integer, Monom>)it.next();
			
			coef = pair.getValue().getCoef() * pair.getKey();
			grad = pair.getKey() - 1;
			
			rez.polinom.put(grad, new Monom(grad, coef));
		}
		return rez;
	}
	
	public Polinom integ()
	{
		Polinom rez = new Polinom();

		Iterator<Entry<Integer, Monom>> it = this.polinom.entrySet().iterator();
		
		while(it.hasNext())
		{
			double coef;
			int grad;
			Map.Entry<Integer, Monom> pair = (Map.Entry<Integer, Monom>)it.next();
			
			coef = pair.getValue().getCoef() / (pair.getKey() + 1);
			grad = pair.getKey() + 1;
			
			rez.polinom.put(grad, new Monom(grad, coef));
		}
		return rez;
	}
}
