package polinoame;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PolinomTest {

	@Test
	public void addTest()
	{
		Polinom firstPolin = new Polinom();
		Polinom secondPolin = new Polinom();
		
		firstPolin.parsePolinom("0x^0");
		secondPolin.parsePolinom("0x^0");
		assertEquals("0", firstPolin.add(secondPolin).toString());
		
		firstPolin.parsePolinom("0x^0");
		secondPolin.parsePolinom("1x^0");
		assertEquals("+1.0x^0", firstPolin.add(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^0");
		secondPolin.parsePolinom("0x^0");
		assertEquals("+1.0x^0", firstPolin.add(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^0");
		secondPolin.parsePolinom("1x^0");
		assertEquals("+2.0x^0", firstPolin.add(secondPolin).toString());
		
		firstPolin.parsePolinom("0x^0");
		secondPolin.parsePolinom("-1x^0");
		assertEquals("-1.0x^0", firstPolin.add(secondPolin).toString());
		
		firstPolin.parsePolinom("-1x^0");
		secondPolin.parsePolinom("0x^0");
		assertEquals("-1.0x^0", firstPolin.add(secondPolin).toString());
		
		firstPolin.parsePolinom("-1x^0");
		secondPolin.parsePolinom("-1x^0");
		assertEquals("-2.0x^0", firstPolin.add(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^1 + 1x^0");
		secondPolin.parsePolinom("1x^1 + 1x^0");
		assertEquals("+2.0x^1+2.0x^0", firstPolin.add(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^1 + 1x^0");
		secondPolin.parsePolinom("-1x^1 - 1x^0");
		assertEquals("0", firstPolin.add(secondPolin).toString());
		
		firstPolin.parsePolinom("72x^50 + 56x^32 + 20x^20 + 12x^5 + 7x^0 + 1x^-1");
		secondPolin.parsePolinom("32x^-1 + 31x^5 + 20x^7 - 30x^50");
		assertEquals("+42.0x^50+56.0x^32+20.0x^20+20.0x^7+43.0x^5+7.0x^0+33.0x^-1", firstPolin.add(secondPolin).toString());
	}
	
	@Test
	public void difTest()
	{
		Polinom firstPolin = new Polinom();
		Polinom secondPolin = new Polinom();
		
		firstPolin.parsePolinom("0x^0");
		secondPolin.parsePolinom("0x^0");
		assertEquals("0", firstPolin.dif(secondPolin).toString());
		
		firstPolin.parsePolinom("0x^0");
		secondPolin.parsePolinom("1x^0");
		assertEquals("-1.0x^0", firstPolin.dif(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^0");
		secondPolin.parsePolinom("0x^0");
		assertEquals("+1.0x^0", firstPolin.dif(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^0");
		secondPolin.parsePolinom("1x^0");
		assertEquals("0", firstPolin.dif(secondPolin).toString());
		
		firstPolin.parsePolinom("0x^0");
		secondPolin.parsePolinom("-1x^0");
		assertEquals("+1.0x^0", firstPolin.dif(secondPolin).toString());
		
		firstPolin.parsePolinom("-1x^0");
		secondPolin.parsePolinom("0x^0");
		assertEquals("-1.0x^0", firstPolin.dif(secondPolin).toString());
		
		firstPolin.parsePolinom("-1x^0");
		secondPolin.parsePolinom("-1x^0");
		assertEquals("0", firstPolin.dif(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^1 + 1x^0");
		secondPolin.parsePolinom("1x^1 + 1x^0");
		assertEquals("0", firstPolin.dif(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^1 + 1x^0");
		secondPolin.parsePolinom("-1x^1 - 1x^0");
		assertEquals("+2.0x^1+2.0x^0", firstPolin.dif(secondPolin).toString());
		
		firstPolin.parsePolinom("72x^50 + 56x^32 + 20x^20 + 12x^5 + 7x^0 + 1x^-1");
		secondPolin.parsePolinom("32x^-1 + 31x^5 + 20x^7 - 30x^50");
		assertEquals("+102.0x^50+56.0x^32+20.0x^20-20.0x^7-19.0x^5+7.0x^0-31.0x^-1", firstPolin.dif(secondPolin).toString());
	}

	@Test
	public void mulTest()
	{
		Polinom firstPolin = new Polinom();
		Polinom secondPolin = new Polinom();
		
		firstPolin.parsePolinom("0x^0");
		secondPolin.parsePolinom("0x^0");
		assertEquals("0", firstPolin.mul(secondPolin).toString());
		
		firstPolin.parsePolinom("0x^0");
		secondPolin.parsePolinom("1x^0");
		assertEquals("0", firstPolin.mul(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^0");
		secondPolin.parsePolinom("0x^0");
		assertEquals("0", firstPolin.mul(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^0");
		secondPolin.parsePolinom("1x^0");
		assertEquals("+1.0x^0", firstPolin.mul(secondPolin).toString());
		
		firstPolin.parsePolinom("0x^0");
		secondPolin.parsePolinom("-1x^0");
		assertEquals("0", firstPolin.mul(secondPolin).toString());
		
		firstPolin.parsePolinom("-1x^0");
		secondPolin.parsePolinom("0x^0");
		assertEquals("0", firstPolin.mul(secondPolin).toString());
		
		firstPolin.parsePolinom("-1x^0");
		secondPolin.parsePolinom("-1x^0");
		assertEquals("+1.0x^0", firstPolin.mul(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^1 + 1x^0");
		secondPolin.parsePolinom("1x^1 + 1x^0");
		assertEquals("+1.0x^2+2.0x^1+1.0x^0", firstPolin.mul(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^1 + 1x^0");
		secondPolin.parsePolinom("-1x^1 - 1x^0");
		assertEquals("-1.0x^2-2.0x^1-1.0x^0", firstPolin.mul(secondPolin).toString());
		
		firstPolin.parsePolinom("72x^50 + 56x^32 + 1x^-1");
		secondPolin.parsePolinom("20x^7 - 30x^50");
		assertEquals("-2160.0x^100-1680.0x^82+1440.0x^57-30.0x^49+1120.0x^39+20.0x^6", firstPolin.mul(secondPolin).toString());
	}
	
	@Test
	public void divTest()
	{
		Polinom firstPolin = new Polinom();
		Polinom secondPolin = new Polinom();
		
		firstPolin.parsePolinom("0x^0");
		secondPolin.parsePolinom("0x^0");
		assertEquals("0", firstPolin.div(secondPolin).toString());
		
		firstPolin.parsePolinom("0x^0");
		secondPolin.parsePolinom("1x^0");
		assertEquals("0", firstPolin.div(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^2");
		secondPolin.parsePolinom("1x^0");
		assertEquals("+1.0x^2", firstPolin.div(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^0");
		secondPolin.parsePolinom("1x^0");
		assertEquals("+1.0x^0", firstPolin.div(secondPolin).toString());
		
		firstPolin.parsePolinom("0x^0");
		secondPolin.parsePolinom("-1x^0");
		assertEquals("0", firstPolin.div(secondPolin).toString());
		
		firstPolin.parsePolinom("-1x^2");
		secondPolin.parsePolinom("-1x^0");
		assertEquals("+1.0x^2", firstPolin.div(secondPolin).toString());
		
		firstPolin.parsePolinom("-1x^0");
		secondPolin.parsePolinom("-1x^0");
		assertEquals("+1.0x^0", firstPolin.div(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^1 + 1x^0");
		secondPolin.parsePolinom("1x^1 + 1x^0");
		assertEquals("+1.0x^0", firstPolin.div(secondPolin).toString());
		
		firstPolin.parsePolinom("1x^1 + 1x^0");
		secondPolin.parsePolinom("-1x^1 - 1x^0");
		assertEquals("-1.0x^0", firstPolin.div(secondPolin).toString());
		
		firstPolin.parsePolinom("72x^50 + 56x^32 + 1x^-1");
		secondPolin.parsePolinom("20x^7 - 30x^50");
		assertEquals("-2.4x^0", firstPolin.div(secondPolin).toString());
	}
	
	@Test
	public void derivTest()
	{
		Polinom firstPolin = new Polinom();
		
		firstPolin.parsePolinom("0x^0");
		assertEquals("0", firstPolin.deriv().toString());
		
		firstPolin.parsePolinom("1x^1");
		assertEquals("+1.0x^0", firstPolin.deriv().toString());
		
		firstPolin.parsePolinom("1x^2");
		assertEquals("+2.0x^1", firstPolin.deriv().toString());
		
		firstPolin.parsePolinom("1x^-1");
		assertEquals("-1.0x^-2", firstPolin.deriv().toString());
		
		firstPolin.parsePolinom("3x^4");
		assertEquals("+12.0x^3", firstPolin.deriv().toString());
		
		firstPolin.parsePolinom("-1x^2");
		assertEquals("-2.0x^1", firstPolin.deriv().toString());
		
		firstPolin.parsePolinom("-1x^-1");
		assertEquals("+1.0x^-2", firstPolin.deriv().toString());
		
		firstPolin.parsePolinom("1x^1 + 1x^0");
		assertEquals("+1.0x^0", firstPolin.deriv().toString());
		
		firstPolin.parsePolinom("-1x^1 - 1x^0");
		assertEquals("-1.0x^0", firstPolin.deriv().toString());
		
		firstPolin.parsePolinom("72x^50 + 56x^32 + 1x^-1");
		assertEquals("+3600.0x^49+1792.0x^31-1.0x^-2", firstPolin.deriv().toString());
	}
	
	@Test
	public void integTest()
	{
		Polinom firstPolin = new Polinom();
		
		firstPolin.parsePolinom("0x^0");
		assertEquals("0", firstPolin.integ().toString());
		
		firstPolin.parsePolinom("1x^1");
		assertEquals("+0.5x^2", firstPolin.integ().toString());
		
		firstPolin.parsePolinom("1x^3");
		assertEquals("+0.25x^4", firstPolin.integ().toString());
		
		firstPolin.parsePolinom("1x^0");
		assertEquals("+1.0x^1", firstPolin.integ().toString());
		
		firstPolin.parsePolinom("3x^3");
		assertEquals("+0.75x^4", firstPolin.integ().toString());
		
		firstPolin.parsePolinom("-1x^1");
		assertEquals("-0.5x^2", firstPolin.integ().toString());
		
		firstPolin.parsePolinom("-1x^-3");
		assertEquals("+0.5x^-2", firstPolin.integ().toString());
		
		firstPolin.parsePolinom("1x^1 + 1x^0");
		assertEquals("+0.5x^2+1.0x^1", firstPolin.integ().toString());
		
		firstPolin.parsePolinom("-1x^1 - 1x^0");
		assertEquals("-0.5x^2-1.0x^1", firstPolin.integ().toString());
		
		firstPolin.parsePolinom("102x^50 + 66x^32 + 1x^0");
		assertEquals("+2.0x^51+2.0x^33+1.0x^1", firstPolin.integ().toString());
	}
}
