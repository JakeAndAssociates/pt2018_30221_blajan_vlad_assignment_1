package polinoame;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GUI {
	
	private JFrame meniu = new JFrame("Meniu");
	private JPanel panel = new JPanel();
	private JPanel firstPanel = new JPanel();
	private JPanel secondPanel = new JPanel();
	
	private JLabel firstPolinLabel = new JLabel("primul polinom");
	private JLabel secondPolinLabel = new JLabel("al doilea polinom");
	private JLabel rezPolinLabel = new JLabel();
	private JTextField firstPolinText = new JTextField();
	private JTextField secondPolinText = new JTextField();
	
	public JButton add = new JButton("add");
	public JButton dif = new JButton("dif");
	public JButton mul = new JButton("mul");
	public JButton div = new JButton("div");
	public JButton deriv = new JButton("deriv");
	public JButton integ = new JButton("integ");
	
	public GUI()
	{
		meniu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		meniu.setSize(500, 200);
		
		firstPanel.add(firstPolinLabel);
		firstPanel.add(firstPolinText);
		firstPanel.add(secondPolinLabel);
		firstPanel.add(secondPolinText);
		secondPanel.add(add);
		secondPanel.add(dif);
		secondPanel.add(mul);
		secondPanel.add(div);
		secondPanel.add(deriv);
		secondPanel.add(integ);
		
		firstPanel.setLayout(new BoxLayout(firstPanel, BoxLayout.Y_AXIS));
		secondPanel.setLayout(new FlowLayout());
		panel.add(firstPanel);
		panel.add(secondPanel);
		panel.add(rezPolinLabel);
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		meniu.setContentPane(panel);
		meniu.setVisible(true);
	}
	
	public void add()
	{
		Polinom firstPolin = new Polinom(firstPolinText.getText().toString());
		Polinom secondPolin = new Polinom(secondPolinText.getText().toString());
		
		Polinom rez = firstPolin.add(secondPolin);
		
		rezPolinLabel.setText("rezultat : " + rez.toString());
	}
	
	public void dif()
	{
		Polinom firstPolin = new Polinom(firstPolinText.getText());
		Polinom secondPolin = new Polinom(secondPolinText.getText());
		
		Polinom rez = firstPolin.dif(secondPolin);
		
		rezPolinLabel.setText("rezultat : " + rez.toString());
	}
	
	public void mul()
	{
		Polinom firstPolin = new Polinom(firstPolinText.getText());
		Polinom secondPolin = new Polinom(secondPolinText.getText());
		
		Polinom rez = firstPolin.mul(secondPolin);
		
		rezPolinLabel.setText("rezultat : " + rez.toString());
	}
	
	public void div()
	{
		Polinom firstPolin = new Polinom(firstPolinText.getText());
		Polinom secondPolin = new Polinom(secondPolinText.getText());
		
		if(secondPolin.polinom.isEmpty())
		{
			rezPolinLabel.setText("impartire la 0");
			return;
		}
		
		Polinom cat = new Polinom();
		Polinom rest = new Polinom();
		
		cat = firstPolin.div(secondPolin);
		rest = firstPolin.dif(secondPolin.mul(cat));
		
		rezPolinLabel.setText("cat : " + cat.toString() + "  rest : " + rest.toString());
	}
	
	public void deriv()
	{
		Polinom firstPolin = new Polinom(firstPolinText.getText());
		
		Polinom rez = firstPolin.deriv();
		
		rezPolinLabel.setText("rezultat : " + rez.toString());
	}
	
	public void integ()
	{
		Polinom firstPolin = new Polinom(firstPolinText.getText());
		
		Polinom rez = firstPolin.integ();
		
		rezPolinLabel.setText("rezultat : " + rez.toString());
	}
}
