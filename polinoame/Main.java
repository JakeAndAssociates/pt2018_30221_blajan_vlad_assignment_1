package polinoame;

public class Main 
{

	public static void main(String[] args) 
	{
		GUI gui = new GUI();
		
		gui.add.addActionListener(e->{
			gui.add();
		});
		
		gui.dif.addActionListener(e->{
			gui.dif();
		});
	
		gui.mul.addActionListener(e->{
			gui.mul();
		});
		
		gui.div.addActionListener(e->{
			gui.div();
		});
		
		gui.deriv.addActionListener(e->{
			gui.deriv();
		});
		
		gui.integ.addActionListener(e->{
			gui.integ();
		});
	}

}
