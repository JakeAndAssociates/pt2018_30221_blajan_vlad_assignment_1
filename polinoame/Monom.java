package polinoame;

public class Monom {

	private int grad;
	private double coef;
	
	
	public Monom(int grad, double coef)
	{
		this.coef = coef;
		this.grad = grad;
	}
	
	public void setGrad(int grad)
	{
		this.grad = grad;
	}
	
	public void setCoef(double coef)
	{
		this.coef = coef;
	}
	
	public int getGrad()
	{
		return this.grad;
	}
	
	public double getCoef()
	{
		return this.coef;
	}
}
